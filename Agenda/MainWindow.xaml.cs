﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Data.SqlClient;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;

namespace Agenda
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    /// MODEL INTERFACE (CONTROLS)
    /// 
    public partial class MainWindow : Window
    {
        //Ventana inicial

        public MainWindow()
        {
            InitializeComponent();
        }

        //Controladores de Eventos (Click)

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Conección conectar = new Conección();
            Conección.conectar.Click += ValidarSesión_Click;
            Conección.IpPredefinida.Click += IpPredefinida;
            conectar.Show();
        }

        private void Button1_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        public static void IpPredefinida(object sender,RoutedEventArgs e)
        {
            NoteBook.cadenaConexión = ConfigurationManager.AppSettings["0rigen"];
        }
        public static void ValidarSesión_Click(object sender, RoutedEventArgs e)
        {
            if (Conección.usuario.Text == "usuario" && Conección.contraseña.Text == "cipsa")
            {
                Conectado conectado = new Conectado();
                Conectado.ListaEmpleados.Click += ListaEmpleados_Click;
                Conectado.BuscarEmpleado.Click += BuscarEmpleado_Click;
                Conectado.Gestión.Click += Gestión_Click;
                try
                {
                    Contacto berón = new Contacto()
                    {
                        DNI = "Y7895498",
                        Nombre = "Adonis",
                        Apellido = "Villalba",
                        Teléfono = "0984484875",
                        Nacimiento = "13/03/1996"
                    };
                    NoteBook note = new NoteBook();
                    note.Contactos.Add(berón);
                    note.SaveChanges();
                    NoteBook.NuevoUsuario();
                }
                catch (Exception i)
                {
                    MessageBox.Show(i.Message);
                }
                conectado.Show();
            }
            else
            {
                MessageBox.Show("El nombre de usuario o contraseña no son válidos");
            }
        }

        public static void ListaEmpleados_Click(object sender,RoutedEventArgs e)
        {
            NoteBook note = new NoteBook();
            ListaEmpleados.data.ItemsSource = note.Contactos.ToList();
            ListaEmpleados.data.CanUserAddRows = false;
            ListaEmpleados.data.CanUserDeleteRows = false;
            ListaEmpleados empleados = new ListaEmpleados();
            empleados.Show();
        }

        public static void BuscarEmpleado_Click(object sender, RoutedEventArgs e)
        {
            BuscarEmpleado buscarEmpleado = new BuscarEmpleado();
            BuscarEmpleado.buscarEmpleado.Click += Buscar_Click;
            buscarEmpleado.Show();
        }

        public static void Buscar_Click(object sender, RoutedEventArgs e)
        {
            NoteBook note = new NoteBook();
            IQueryable<Contacto> contact = from contacto in note.Contactos where contacto.DNI == BuscarEmpleado.DNI.Text
                                            select contacto;
            Contacto empleado = contact.First();
            BuscarEmpleado.nombre.Content = empleado.Nombre;
            BuscarEmpleado.apellido.Content = empleado.Apellido;
            BuscarEmpleado.teléfono.Content = empleado.Teléfono;
            BuscarEmpleado.nacimiento.Content = empleado.Nacimiento;

        }

        static Contacto contactos = null;
        public static void Gestión_Click(object sender, RoutedEventArgs e)
        {
            Gestión gestión = new Gestión();

            //Restricción DataGrid
            NoteBook note = new NoteBook();
            Reload();
            Gestión.data.SelectionChanged += CambioSelección;
            Gestión.data.CanUserAddRows = false;
            Gestión.data.CanUserDeleteRows = false;
            Gestión.data.AutoGenerateColumns = false;
            Gestión.data.Columns.Add(new DataGridTextColumn
            {
                Header = "DNI",
                Binding = new Binding("DNI")
            });
            Gestión.data.Columns.Add(new DataGridTextColumn
            {
                Header = "Nombre",
                Binding = new Binding("Nombre")
            });
            Gestión.data.Columns.Add(new DataGridTextColumn
            {
                Header = "Apellido",
                Binding = new Binding("Apellido")
            });
            gestión.Show();
            void Reload()
            {
                Gestión.data.ItemsSource = note.Contactos.ToList();
            }

                //Asignación de controles botones de ventana gestión

                Gestión.guardar.Click += Guardar_Click;
                Gestión.alta.Click += DarDeAlta;
                Gestión.baja.Click += DarDeBaja;

             //Controlador evento selección de una fila 

            void CambioSelección(object sent, SelectionChangedEventArgs i)
            {
                if (Gestión.data.CurrentItem != null)
                {
                    contactos = Gestión.data.CurrentItem as Contacto;
                    Gestión.nombre.Text = contactos.Nombre;
                    Gestión.apellido.Text = contactos.Apellido;
                    Gestión.teléfono.Text = contactos.Teléfono;
                    Gestión.nacimiento.Text = contactos.Nacimiento;
                    Gestión.DNI.Text = contactos.DNI;
                }
                else
                {
                    Gestión.nombre.Text = "";
                    Gestión.apellido.Text = "";
                    Gestión.teléfono.Text = "";
                    Gestión.nacimiento.Text = "";
                    Gestión.DNI.Text = "";
                }
            }

                //Controlador para guardar cambios

                void Guardar_Click(object send, RoutedEventArgs a)
                {
                    note.Contactos.Remove(contactos);
                    Contacto contacto = new Contacto()
                    {
                        DNI = Gestión.DNI.Text,
                        Nombre = Gestión.nombre.Text,
                        Apellido = Gestión.apellido.Text,
                        Teléfono = Gestión.teléfono.Text,
                        Nacimiento = Gestión.nacimiento.Text
                    };
                    note.Contactos.Add(contacto);
                    note.SaveChanges();
                    Reload();
                }

                //Controlador para eliminar usuario

                void DarDeBaja(object sending, RoutedEventArgs u)
                {
                    try
                    {
                        note.Contactos.Remove(contactos);
                        note.SaveChanges();
                        Reload();
                    }
                    catch (Exception ex)
                    {
                    }
                }
        }       

        //Controlador evento agregar usuario

        public static void DarDeAlta(object sender,RoutedEventArgs e)
        {
            try
            {
                Contacto contacto = new Contacto()
                {
                    DNI = Gestión.DNI.Text,
                    Nombre = Gestión.nombre.Text,
                    Apellido = Gestión.apellido.Text,
                    Teléfono = Gestión.teléfono.Text,
                    Nacimiento = Gestión.nacimiento.Text
                };
                NoteBook note = new NoteBook();
                note.Contactos.Add(contacto);
                note.SaveChanges();
                Gestión.data.ItemsSource = note.Contactos.ToList();
            }
            catch
            {
            }
        }
    }

    //DATA INTERFACE (DB-CONTENT)

    //Diseño de tabla, columnas y conexión

    public class NoteBook : DbContext
    {
        static public string cadenaConexión = $"data source={Conección.direcciónIp.Text};initial catalog=AGENDA;integrated security=true; " +
            $"Connection Timeout=10";
        public NoteBook():base 
            (cadenaConexión)
        {

        }
        public DbSet<Contacto> Contactos { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Contacto>().ToTable("Empleado");
        }
        public static void NuevoUsuario()
        {
            using (SqlConnection conexión = new SqlConnection(cadenaConexión))
            {
                conexión.Open();
                SqlCommand nuevoUsuario = new SqlCommand("Create login usuario with password ='cipsa'", conexión);
                nuevoUsuario.ExecuteNonQuery();
                nuevoUsuario.CommandText = "Create user usuario for login usuario";
                nuevoUsuario.ExecuteNonQuery();
                nuevoUsuario.CommandText = "Alter role db_owner add member usuario";
            }
        }
    }


    public class Contacto
    {
        [Key]
        [StringLength(9)]
        public string DNI { get; set; }
        [Required]
        [StringLength(15)]
        public string Nombre { get; set; }
        [Required]
        [StringLength(30)]
        public string Apellido { get; set; }
        [StringLength(12)]
        public string Teléfono { get; set; }
        [DataType(DataType.DateTime)]
        public string Nacimiento { get; set; }
    }

    //VISUAL INTERFACE (WINDOWS)

    public class Conección :Window
    {

        //Ventana para iniciar sesión

        public Conección()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            Height = 450;
            Width = 800;
            Grid grid = new Grid();
            grid.Children.Add(direcciónIp);
            grid.Children.Add(Ip);
            grid.Children.Add(usuario);
            grid.Children.Add(contraseña);
            grid.Children.Add(Login);
            grid.Children.Add(Password);
            grid.Children.Add(conectar);
            grid.Children.Add(IpPredefinida);
            Content = grid;
        }

        //Etiquetas

        public static Label Ip = Etiqueta(new Thickness(10,10,592,368),"Dirección IP de la base de datos"), 
            Login = Etiqueta(new Thickness(10,110,592,268),"Nombre de usuario"), 
            Password = Etiqueta(new Thickness(10,210,592,168),"Contraseña");

        //Entradas de datos

        public static TextBox direcciónIp = Entrada(new Thickness(10, 60, 592, 318)), 
            usuario = Entrada(new Thickness(10, 160, 592, 218)),
            contraseña = Entrada(new Thickness(10, 260, 592, 118));

        //Botones
                
        public static Button conectar = Botón(new Thickness(582, 358, 10, 10), "Conectar..."),
            IpPredefinida=Botón(new Thickness(582,308,10,60),"Dirección Ip por defecto");

        //Métodos para crear controles

        public static Button Botón(Thickness posición,string contenido)
        {
            Button botón = new Button();
            botón.Margin = posición;
            botón.Content = contenido;
            return botón;
        }
        public static Label Etiqueta(Thickness posición,string contenido)
        {
            Label etiqueta = new Label();
            etiqueta.Margin = posición;
            etiqueta.Content = contenido;
            return etiqueta;
        }
        public static TextBox Entrada(Thickness posición)
        {
            TextBox entrada = new TextBox();
            entrada.Margin = posición;
            return entrada;
        }
    }
    public class Conectado : Window
    {

        //Ventana de botones para realizar acciones sobre la tabla Empleado

        public Conectado()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            Width = 350;
            Height = 200;
            Grid grid = new Grid();
            grid.Children.Add(ListaEmpleados);
            grid.Children.Add(BuscarEmpleado);
            grid.Children.Add(Gestión);
            Content = grid;
        }

        //Botónes

        public static Button ListaEmpleados = Conección.Botón(new Thickness(30, 10, 30, 118), "Lista de Empleados..."),
            BuscarEmpleado = Conección.Botón(new Thickness(30, 60, 30, 68), "Datos de un Empleado..."),
            Gestión = Conección.Botón(new Thickness(30, 110, 30, 18), "Gestion de Empleados");
    }

    public class ListaEmpleados : Window
    {

        //Ventana con la lista de empleados

        public ListaEmpleados()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            Width = 308;
            Height = 332;
            Content = data;
        }
        public static DataGrid data = new DataGrid();
    }

    public class BuscarEmpleado : Window
    {

        //Ventana para buscar un empleado por su DNI
        public BuscarEmpleado()
        {
            WindowStartupLocation=WindowStartupLocation.CenterScreen;
            Width = 438;
            Height = 262;
            Grid grid = new Grid();
            grid.Children.Add(nombre);
            grid.Children.Add(DNI);
            grid.Children.Add(apellido);
            grid.Children.Add(teléfono);
            grid.Children.Add(DNILabel);
            grid.Children.Add(nacimiento);
            grid.Children.Add(buscarEmpleado);
            Content = grid;
        }
        public static Button buscarEmpleado = Conección.Botón(new Thickness(300,10,10,180), "Buscar Empleado");
        public static Label DNILabel = Conección.Etiqueta(new Thickness(10,10,220,180), "DNI del Empleado"),
            nombre=Conección.Etiqueta(new Thickness(10,110,220,80),"Nombre del Empleado"),
            apellido=Conección.Etiqueta(new Thickness(220,110,10,80),"Apellido del Empleado"),
            teléfono=Conección.Etiqueta(new Thickness(10,160,220,30),"Teléfono del Empleado"),
            nacimiento=Conección.Etiqueta(new Thickness(220,160,10,30),"Nacimiento del Empleado");
        public static TextBox DNI = Conección.Entrada(new Thickness(10,60,220,130));
    }
    public class Gestión : Window
    {
        public Gestión()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            Width = 658;
            Height = 432;
            Grid grid = new Grid();
            data.Margin = new Thickness(150, 10, 150, 200);
            grid.Children.Add(nombre);
            grid.Children.Add(apellido);
            grid.Children.Add(teléfono);
            grid.Children.Add(nacimiento);
            grid.Children.Add(DNI);
            grid.Children.Add(name);
            grid.Children.Add(lastname);
            grid.Children.Add(phone);
            grid.Children.Add(born);
            grid.Children.Add(guardar);
            grid.Children.Add(cancelar);
            grid.Children.Add(alta);
            grid.Children.Add(baja);
            grid.Children.Add(data);
            grid.Children.Add(dni);
            Content = grid;
        }
        public static DataGrid data = new DataGrid();        
        public static TextBox nombre = Conección.Entrada(new Thickness(120,210,330,150)),
            apellido=Conección.Entrada(new Thickness(450, 210, 10, 150)), 
            teléfono=Conección.Entrada(new Thickness(120, 260, 330, 100)),
            nacimiento=Conección.Entrada(new Thickness(450, 260, 10, 100)), 
            DNI =Conección.Entrada(new Thickness(120, 310, 330, 50));
        public static Label name = Conección.Etiqueta(new Thickness(10,210,540,150), "Nombre: "),
            lastname=Conección.Etiqueta(new Thickness(340,210,220,150),"Apellido: "), 
            born=Conección.Etiqueta(new Thickness(340, 260, 10, 100),"Nacimiento: "),
            phone=Conección.Etiqueta(new Thickness(10, 260, 540, 100),"Teléfono: "), 
            dni=Conección.Etiqueta(new Thickness(10, 310, 540, 50),"DNI: ");
        public static Button guardar = Conección.Botón(new Thickness(450, 310, 10, 50), "Guardar empleado"),
            cancelar = Conección.Botón(new Thickness(340,310,210,50), "Cancelar"), 
            alta = Conección.Botón(new Thickness(10,360,520,0), "Dar de ALta"),
            baja = Conección.Botón(new Thickness(520,360,10,0),"Dar de Baja");       
    }
}
